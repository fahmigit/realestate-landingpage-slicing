module.exports = {
  content: ["./*.{html,js}"],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Poppins", "sans-serif"],
      },
      keyframes: {
        naikturun: {
          "0%, 100%": {
            transform: "translateY(-10px)",
          },
          "50%": {
            transform: "translateY(20px)",
          },
        },
        kanankiri: {
          "0%, 100%": {
            transform: "translateX(-10px)",
          },
          "50%": {
            transform: "translateX(20px)",
          },
        },
        kanankiripol: {
          "0%, 100%": {
            transform: "translateX(0px)",
          },
          "50%": {
            transform: "translateX(1000px)",
          },
        },
        kanankirimediumpol: {
          "0%, 100%": {
            transform: "translateX(-50px)",
          },
          "50%": {
            transform: "translateX(100px)",
          },
        },
      },
      animation: {
        "bounce-slow": "bounce 3s infinite",
        "naik-turun": "naikturun 5s ease-in-out infinite",
        "kanan-kiri": "kanankiri 5s ease-in-out infinite",
        "kanan-kiri-pol": "kanankiripol 20s ease-in-out infinite",
        "kanan-kiri-medium-pol": "kanankirimediumpol 3s ease-in-out infinite",
      },
    },
  },
  plugins: [],
};
